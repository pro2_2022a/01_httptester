package dataModel;

public abstract class TestResult
{
    public abstract String GetSummary();
}
