package dataModel;

import java.util.ArrayList;

public class TestCollection
{
    public TestCollection(String url)
    {
        this.url = url;
    }

    private String url;
    public ArrayList<TestResult> Results = new ArrayList<>();

    public String getUrl()
    {
        return url;
    }
}
