package experiments;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Elevation
{
    public static int RealElevation(float lat, float lon) throws IOException
    {
        Properties config = new Properties();
        config.load(new FileInputStream("app.config"));
        try
        {
            URL url1 = new URL(config.getProperty("elevationApi") + "?locations=" + lat + "," + lon);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            java.util.Scanner responseScanner = new java.util.Scanner(conn.getInputStream()).useDelimiter("\\A");
            String responseString = responseScanner.next();

            Pattern regexPattern = Pattern.compile("\"elevation\": ([0-9]*)");
            Matcher regexMatcher = regexPattern.matcher(responseString);
            String elevationString = "0";
            if (regexMatcher.find())
            {
                elevationString = regexMatcher.group(1);
            }

            return Integer.parseInt(elevationString);
        }
        catch (IOException e)
        {
            System.out.println(e);
            return 0;
        }
    }
}
