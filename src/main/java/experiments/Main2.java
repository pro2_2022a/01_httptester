package experiments;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main2
{
    public static void main(String[] args) throws IOException, InterruptedException {

        String data="127.0.15.15";
        String[] strings= data.split("\\.");

        String userDir =System.getProperty("user.dir");
        Path path = Paths.get(userDir,"output.pro2");
        OutputStream outputStream = new FileOutputStream(path.toString());
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

        for(int i=0; i<100; i++)
        {
            int elevation = Elevation.RealElevation(
                    48.5f, 17.5f+i*0.01f);
            System.out.println(elevation);
            dataOutputStream.writeInt(elevation);
            int sleep = 200;
            Thread.sleep(sleep);
        }

        dataOutputStream.flush();
        dataOutputStream.close();
    }
}
